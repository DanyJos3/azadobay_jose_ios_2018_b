//
//  NumbersPair.swift
//  Numeros_Pares
//
//  Created by Jose Azadobay on 27/11/18.
//  Copyright © 2018 Jose Azadobay. All rights reserved.
//

import UIKit

class NumbersPair: UIViewController {

    @IBOutlet weak var number: UITextField!
    
    @IBOutlet weak var resultado: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    
    
    @IBAction func pairsButton(_ sender: Any) {
        
        let n: Int? = Int(number.text!);
        let numero : Int = n!
        
        var valores: [String] = []
        
        for valor in Swift.stride(from: 0, to: (numero+1), by: 2) {
            valores.append("\(valor)")
        }
        
        resultado.text = "\(valores)";
      
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func regresar(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
