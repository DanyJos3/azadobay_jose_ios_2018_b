//
//  ViewController.swift
//  Numeros_Pares
//
//  Created by Jose Azadobay on 27/11/18.
//  Copyright © 2018 Jose Azadobay. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController {
    
    @IBOutlet weak var username: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func login(_ sender: Any) {
        let user = username.text!
        let pass = password.text! //El signo de interrogacion sirve para evitar los errores de nulos
        
        Auth.auth().signIn(withEmail: user, password: pass) { (data, error) in
            
            if let err = error { // Si error es null el if falla es falso
                print (err)
                return
            }
            
            
            // Esta linea ejecuta la transicion con que identificador
            // El self hace referencia a la clase principal al view controller.
            self.performSegue(withIdentifier: "numberSegue", sender: self )
        }
    }
    
    
}
